import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class SceneManagerSc extends cc.Component {

    @property(cc.Node)
    StartUI: cc.Node = null;
    @property(cc.Node)
    GameUI: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Msger.on(Msger.on_changeto_start, this.on_changeto_start, this);
        Msger.on(Msger.on_changeto_game, this.on_changeto_game, this);
        this.StartUI.active = true;
        this.GameUI.active = false;
    }

    start() {

    }

    // update (dt) {}

    private on_changeto_game() {
        this.StartUI.active = false;
        this.GameUI.active = true;
        Msger.emit(Msger.on_game_begin);
    }
    private on_changeto_start() {
        this.StartUI.active = true;
        this.GameUI.active = false;
    }
}
