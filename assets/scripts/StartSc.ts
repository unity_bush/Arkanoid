import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class StartUI extends cc.Component {

    @property(cc.Prefab)
    rankUI: cc.Prefab = null;
    @property(cc.Node)
    serviceUI: cc.Node = null;


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    // update (dt) {}
    onclick_start(){
        Msger.emit(Msger.on_changeto_game);
        Msger.emit(Msger.on_play_sound, 1);
    }
    onclick_rank(){
        this.node.addChild(cc.instantiate(this.rankUI));
        Msger.emit(Msger.on_play_sound, 1);
    }
    onclick_sound(){
        Msger.emit(Msger.on_sound_muted);
        Msger.emit(Msger.on_play_sound, 1);
    }
    onclick_service(){
        Msger.emit(Msger.on_play_sound, 1);
        this.serviceUI.active = true;
    }
}
