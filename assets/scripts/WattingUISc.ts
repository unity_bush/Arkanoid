import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class WattingUISc extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.Node)
    waitUI: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Msger.on(Msger.on_show_watting, () => {
            if (this.waitUI) {
                this.waitUI.active = true;
            }
        },this)
        
        Msger.on(Msger.on_hide_watting, () => {
            if (this.waitUI) {
                this.waitUI.active = false;
            }
        },this)
        this.waitUI.active = false;
    }

    start() {

    }
    private timer = 0;
    private index = 0;
    update(dt) {
        this.timer += dt;
        if (this.timer > 1) {
            this.timer = 0;
            this.index += 1;
            this.label.string = "请稍后.";
            for (let i = 0; i < this.index % 3; i++) {
                this.label.string += ".";
            }
        }
    }
}
