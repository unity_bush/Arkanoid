import { Msger } from "./Msger";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundConctrollerSc extends cc.Component {

    // @property(cc.Label)
    // label: cc.Label = null;

    // @property
    // text: string = 'hello';

    // LIFE-CYCLE CALLBACKS:
    public static isMuted = false;
    onLoad() {
        Msger.on(Msger.on_play_sound, this.on_play_sound, this);
        Msger.on(Msger.on_sound_muted, this.on_sound_muted, this);
        
    }
    on_sound_muted(){
        SoundConctrollerSc.isMuted = !SoundConctrollerSc.isMuted;
        if(SoundConctrollerSc.isMuted){
            this.node.getChildByName('bgm').getComponent(cc.AudioSource).volume = 0;
        }else{

            this.node.getChildByName('bgm').getComponent(cc.AudioSource).volume = 1;
        }
    }
    // 
    on_play_sound(e) {
        if(SoundConctrollerSc.isMuted){
            return;
        }
        if(e == 8){
            e = Math.floor(Math.random() * 3) + 8;
        }
        this.node.children[e].getComponent(cc.AudioSource).play();
    }

    // update (dt) {}
}
